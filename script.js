// console.log("Hello world! This is from external JS");

//Writing a comment
	//single line
	//multi-line comment


/* 
	Syntax and Statements

Syntax - set of rules of how codes should be written correctly

Statement - set of instructions, ends with semicolon

*/


// alert("Good afternoon");


/* 
	Variables
		- a container that holds value
*/

	// let myName = "Carine";
	// console.log(myName);

/* Anatomy

	let <variable name>
		-declaration
		-declaring a variable
		- cannot re-declare same variable under same scope

	Example:
		let car;
		console.log(car);

		let car="mercendez";
		console.log(car)

	Initialization
		- initializing a variable with a value

	Example:
		let phone = "iPhone";
		console.log(phone);

	Reassignment
		- assigning new value to a variable using equal operator.

	Example:
		myName="Gab";
		console.log(myName);

	// Why do you think variable is important?
		-reusability

		let brand = "Asus"
		brand = "Mac";

		console.log(brand);
		console.log(brand);
		console.log(brand);
		console.log(brand);

*/

// console.log(lastName);
	// without let keyword, return will be "NOT DEFINED"

// let firstName;
// console.log(firstName);
	// returns "UNDEFINED"


/*	Naming Convention - Best Practice
	
	1. Case sensitive

		let color = "pink";
		let Color = "blue";

		console.log(color);
		console.log(Color);

	2. Variable name must be relevant to its actual value
	
			let value = "Robin";

			let goodThief = "Robin";
			let bird = "Robin";

	3. Camel case notation

			let capitalcityofthephilippines = "Manila City";
			// this is readable
			let capitalCityOfThePhilippines = "Manila City";

	4. Cannot start with @ and numbers

		// Not accepted by JS (causes error):
			// let year3 = 3;
			// console.log(year3);

			// let @year = 3;
			// console.log(@year);


		//acceptable
		let _year = 3;
		console.log(_year);

		let $year = 3;
		console.log($year);

	5. Single and double quotes are accepted
		let flower = "rose";
		console.log(flower);

		let flower2 = 'sunflower';
		console.log(flower2);

		* to avoid closing prematurely/escape

		example:
		console.log("She said 'Hello there!'.");
		console.log('She said, "Hello there!".');

		console.log('Hello, I\'m Carine');

		// ES6 updates
			// template literals - using backticks

		console.log(`"She said "Hello there!".`);
*/

/*
	Constant
		- a type of variable that holds value but it cannot be changed nor reassigned

		keyword: const
*/

	// const PI = 3.14;
	// console.log(PI);

/*	PI = 4.1;
	console.log(PI);
		-reassignment is not allowed
*/
	
/*	
	const boilingPoint = 100;
	console.log(boilingPoint);
	-const declaration must have value, else, it will have error 
*/

// let country = `Philippines`;
// let continent = `Asia`;
// let population = `109.6 million`;

// console.log(country);
// console.log(continent);
// console.log(population);

/*
	Data Types


	1. String - sequence of characters
			  - they are always wrapped with backticks or quotes

				let food = `sinigang`;
				let Name = `Carine Cruz`;

			  - if not quotes or backtics, JS will think of it as another variable

				let x = `Hello`;
				let y = `World`;

				x=y;
				console.log(x);

	2. Number

		let x = 4;
		let y = 8;

		let result = x + y;
		console.log(result);
		//result: 12

		let a = `4`;
		let b = `8`;
		let c = `7`;

		let newResult = a + b +c //concantenate
		console.log(newResult);
		//result: 487

		let secondResult = b - a - c; //subtracted numbers
		console.log(secondResult);

	3. Boolean 
			-values TRUE or FALSE
			-prefix must start with is__, are__, have__, etc

				let isEarly = true;
				console.log(isEarly);
				let areLate = false;
				console.log(areLate);

	4. Undefined
		-variable has been declared, however variable has no value yet
		-empty value

				let job;
				console.log(job);

	5. Null
		-empty value
		-it is used to represent an empty value

	6. BigInt()
		-large integers more than the data can hold

	7. Object
		-one variable can handle/contain several different types of data
		- it is wrapped in curly braces
		- has property and value

			let user = {
				firstName: "Dave",
				lastName: "Supan";
				email: [
					"dsmail.com",
					"dave@gmail.com",
					"dsupan@yahoo.com"],
				age: 16,
				isStudent: true,
				spouse: null
			}
*/


	// ** Special type of object
		// Array
			// collection of related data
			// enclosed with square brackets
	// let fruits = ["apple","banana","strawberry"];
	// let grades = [89,90,92,97,95];

/*
	typeof Operator
		- evaluates what type of data your are working on
		- returns string */

/*		let animal = `dog`;
		let age = 16;
		let isHappy = true;
		let him;
		let spouse = null;
		let admin = {
			name: `Admin`,
			isAdmin: true
		};
		let ave = [83.5,89.6,94.2];

		console.log(typeof animal);
		console.log(typeof age);
		console.log(typeof isHappy);
		console.log(typeof him);
		console.log(typeof spouse);
		console.log(typeof admin);
		console.log(typeof ave);*/

/* FUNCTIONS 
	- reusable, convenient because it helps us save time than repeating the same task over and over again
*/
/*
function sayHello() {
	console.log("Hello World");
	console.log("Hello World");
	console.log("Hello World");
	console.log("Hello World");
	console.log("Hello World");
	console.log("Hello World");
	console.log("Hello World");
	console.log("Hello World");
	console.log("Hello World");
}*/

/*sayHello();
console.log("____________");
sayHello();
console.log("____________");*/

/* 
	Syntax of a function:

	function <fName>(){
		//statement / codeblock;
	}

	Anatomy of a function

	1. Declaration
		-function keyword
		-function name + parenthesis
			- parameters inside the parenthesis
		-curly braces
			-determines its codeblock
			-statements are written inside the codeblock
		-has to be called first

	2. Invocation
		- invoke/calls the function
		- by invoking the function, it executes the codeblock

		- function name + parenthesis
			-arguments inside the parenthesis

*/

			//example:
				//parameter
		/*	function greeting(name){
				console.log(`Hi ${name}!`);
			}

				//argument/s
			greeting("Angelito");
			greeting(`Mauro`); */

			//example:

/*			function getProduct(x,y){
				console.log(`The product of ${x} and ${y} is ${x * y}`)
			}

			getProduct(5,7);
			getProduct(8,3);*/

/*
	Mini Activity

	Create a function to get the total sum of three numbers

	Display the output in the console
*/

/*	function getSum(a,b,c){
		console.log(a+b+c);
		return a+b+c;
		// return `${a+b+c}`
	}


	let total = getSum(1,2,3);*/
	// console.log(typeof total);


// Function with return keyword

/*	function sayName(fName, lName){
		return `Hi, may name is ${fName} ${lName}`
	}


	//first method
	console.log(sayName("Roxanne","Talampas"));

	//second method
	let result = sayName("Amiel", "Canta");
	console.log(result);*/

/*
	Mini Activity
*/

/*function displayReturn(name,age){
//console.log(
			`Hi, I'm ${name}. My age ${age} + 10 is ${age+10}` 
			);
	return (
			`Hi, I'm ${name}. \n My age ${age} + 10 is ${age+10}`
			);
			
}

let sample = displayReturn("Tina",15);
console.log(sample);*/

/*----- Comparison Operators ------
	- compare two operands and returns a logical value (boolean value)

	a. Equality (==)
	b. Inequality (!=)
		-- type coercion applies to == and !=
	c. Strict Equality (===)
	d. Strict Inequality (!==) 
	e. relation
		-greater than (>)
		-less than (<)
		-greater than or equal to (>=)
		-less sthan or equal to (<=)
	*/



/*-- Equality Operator (==) ---
		-sometimes called loose equality operator
		- evaluates the values of the operands
		- checks the data types of operands
			-converts/type coercion if values have different data types
		*/

	//console.log("Jan" == "Jan");
	//console.log(true == true);
	//console.log(true == false);
	//console.log(false == true);
	//console.log(false == false);
	//console.log(null == undefined);
	// console.log(true == 2);
	// console.log(true == "1");
		//parseInt("true") = NaN
		// 1 = NaN
		//true = 1
		// "1" = 1
		// 1 = 1

	/* ---- type coercion -----
		
		parseInt()
			- use to convert a data into an integer/number

		parseFloat()
			- use to convert a data into a number while keeping its decimal places

		.toFixed()
			- sets the number of decimal places, round off
			- returns a string value
	*/

		// console.log(typeof(parseInt("100")));
		// console.log(parseFloat("94.64"));
		// console.log(3.14159265);
		// console.log(3.14159265.tofixed(3));

		// //to set number value
		// console.log(parsefloat(3.14159265.tofixed(3)));

	/* Inequality Operator ( != )
		-not equal operands
	*/

/*		console.log("James" != "Daniel"); //true
		console.log(3.00 !== 3); //false
		console.log(false != true); //true
		console.log(true != false); //true
		console.log(true != "true"); //false 
			//true != (true x 1); NaN (Not a Number)

		console.log(null != undefined); //false
		console.log(false != 1); //true
			// false == 0*/

	/* Strict Equality (===) 
		- compares "sameness of values" & data type
		- no conversion
		- type coercion doesn't apply to this operator
	*/

/*		console.log(1 ===1);  //true
		console.log(2 === "2"); //false
		console.log(true === 1); //false
		console.log(null === undefined);*/

	/* Strict Equality (!==) 
		- compares "sameness of values" & data type whether it is not equal
		- no conversion
		- type coercion doesn't apply to this operator
	*/

/*		console.log(4 !== 4); //false --same value, same type
		console.log("4" !== "four"); //true -different values
		console.log(null !== undefined); //true*/

	/* Relation Operators 

	//Greater than (>)
	console.log(45 > 50); //false
	console.log(50 > 49); //true

	//Greater than or equal to (>=)
	
	console.log(5.00 >= 5); //true
	console.log(70 >= 71); //false

	//less than ( < )
	*/

/*	console.log(45 < 50); //true
	console.log(50 < 49); //false

	//less than or equal to (<=)
	console.log(5.00 <= 5); //true
	console.log(70 <= 71); //false
*/

/*
	Logical Operators
		-evaluates operands
		-returns boolean value

//AND operator (&&)
	//checks whether ALL operands are in true value
	console.log(true && true); //true
	console.log(true && false); //false
	console.log(false && true); //false
	console.log(false && false); //false

	let g = true;
	let h = false;
	let i = 12;

	//first condition
		i < 2 //false
		g == h //false
		h && g //false

//OR operator (||)
	//evaluates to true if either of the operands is true

console.log(true || true); //true
console.log(true || false); //true
console.log(false || true); //true
console.log(false || false); //false

let j = 3;
let k = 4;

console.log(j > 7 || 8 < k); 
//3 > 7 = false
//8 < 4 = false
// false || false
*/

/* NOT operator (!)
	- negates/reverses the value
*/

/*console.log(!true); //false
console.log(!false); //true

let isStudent = true;
let isInstructor = "1";

let isAnswer = !isStudent == isInstructor;
//isStudent --> !true = false
//isInstructor = "1"
//isInstructor = 1
//isStudent --> false = 0
//0 == 1
//final answer: false
*/

/*
	without using the browser console, try to analyze the logic manually.

	Q: What is the value of isTralse?

	let isTrue = true;
	let isFalse = false;

	let isTralse = !( !isFalse != !iFalse && isFalse !== isTrue || !isTrue != !isFalse && isFalse != !isTrue)

	solution
	let isTralse = !( (!isFalse != !iFalse) && (isFalse !== isTrue) || (!isTrue != !isFalse) && (isFalse != !isTrue)

	//true != true 	--> false
	//false !== true--> true
	//false != true --> true
	//false != false--> false

	//false && true || true && false
	//false  || false
	//false
	//true

*/

